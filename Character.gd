extends KinematicBody

const MINE_RANGE = 3.0

export(int) var num = 1

# Stats
var movement_level = 1
var attack_level = 1
var mining_level = 1

var max_health = 100
var health = 100
var selected = false

var path = null
var cooldown = 0.0
var mining_cooldown = 0.0
var mining_target = null

# Current order
var order = null
var order_target = null

const Shot = preload("res://Shot.tscn")

func get_movement_speed():
	return 10.0 * pow(1.1, movement_level - 1)

func get_attack_range():
	return 30.0 * pow(1.2, attack_level - 1)

func get_attack_damage():
	return 15.0 * pow(1.4, attack_level - 1)

func get_attack_cooldown():
	return 1.0 * pow(0.92, attack_level - 1)

func get_mining_time():
	# Amount picked up per mining cycle
	return 2.0 * pow(0.8, mining_level - 1)

func _set_order_target(target):
	# Connects to the "tree_exiting" signal of the target to deselect when it gets deleted
	if order_target:
		order_target.disconnect("tree_exiting", self, "_unset_target")
	order_target = target
	if order_target:
		order_target.connect("tree_exiting", self, "_unset_target")

func _unset_target():
	if order_target:
		# Our target got deleted, unset order
		order = null
		order_target.disconnect("tree_exiting", self, "_unset_target")
		order_target = null

func move_to(target):
	if health <= 0.0:
		return

	var map = $"..".map

	path = null
	_unset_target()
	order = "move"
	print("%s move_to %s" % [self, target])

	# Path finding
	map = $"..".map
	path = map.get_simple_path(translation, target)

func pick_up_resources(target):
	if health <= 0.0:
		return

	path = null
	order = "pickup"
	_set_order_target(target)
	print("%s pick up %s" % [self, target])

func attack_enemy(target):
	if health <= 0.0:
		return

	path = null
	order = "attack"
	_set_order_target(target)
	print("%s attack %s" % [self, target])

func damage(amount):
	if health > 0.0:
		health -= amount
		if health <= 0.0:
			$AnimationPlayer.play("Death")

func _physics_process(delta):
	if health <= 0.0:
		return

	var map = $"..".map

	mining_cooldown -= delta
	cooldown -= delta

	if mining_cooldown > 0.0:
		# Can't do anything until mining is done
		return

	if mining_target and mining_cooldown <= 0.0:
		# Mining finished
		mining_target.pick_up()
		mining_target = null

	if order == "attack":
		var max_range = get_attack_range()
		if (order_target.translation - translation).length_squared() <= max_range * max_range:
			path = null
			attack(order_target)
		elif not path:
			# Find a path to approach
			path = map.get_simple_path(translation, order_target.translation)

	if order == "pickup":
		if order_target.value <= 0.0:
			path = null
			print("Order target mined out, clearing")
			order = null
			_unset_target()
		elif (order_target.translation - translation).length_squared() <= MINE_RANGE * MINE_RANGE:
			path = null
			# Start mining
			mining_cooldown = get_mining_time()
			mining_target = order_target
		elif not path:
			# Find a path to approach
			path = map.get_simple_path(translation, order_target.translation)

	if path != null and path.size() > 0:
		var step = delta * get_movement_speed()
		var next_hop = path[0]
		var direction = next_hop - translation

		# Don't overshoot
		if step > direction.length():
			step = direction.length()
			path.remove(0)

		$character/AnimationPlayer.play("Walk")
		move_and_slide(direction.normalized() * step / delta)

		# Look forward
		direction.y = 0
		if direction.length_squared() > 0.001:
			look_at(translation + direction, Vector3(0, 1, 0))
	else:
		path = null
		$character/AnimationPlayer.play("Idle")

		# Find someone to attack
		var best_sq_dist = INF
		var best_enemy = null
		for enemy in get_tree().get_nodes_in_group("EnemyTargets"):
			var sq_dist = (enemy.translation - translation).length_squared()
			if sq_dist < best_sq_dist:
				best_enemy = enemy
				best_sq_dist = sq_dist

		var max_range = get_attack_range()
		if best_sq_dist < max_range * max_range:
			attack(best_enemy)

func attack(enemy):
	look_at(enemy.translation, Vector3(0, 1, 0))
	if cooldown > 0.0:
		return

	# Check that there are no walls in the way
	# Hack: Walls are 8 units high so check there
	var space_state = get_world().direct_space_state
	var result = space_state.intersect_ray(
		Vector3(translation.x, 7.9, translation.z),
		Vector3(enemy.translation.x, 7.9, enemy.translation.z),
		[]
	)
	if result.get("collider"):
		return

	var shot = Shot.instance()
	get_parent().add_child(shot)
	shot.setup(translation, enemy.translation)
	enemy.damage(get_attack_damage())

	cooldown = get_attack_cooldown()
