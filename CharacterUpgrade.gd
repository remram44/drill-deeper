extends Control

export(int) var num = 1 setget _set_num

signal resource_consumed()

onready var game = $"../../../.."

func _set_num(new_num):
	num = new_num
	if is_inside_tree():
		$Label.text = "Upgrade character %d" % num

func _ready():
	$Label.text = "Upgrade character %d" % num

func update_info():
	var character = game.characters[num - 1]
	if character.health > 0.0:
		visible = true
		$Attack.text = "Upgrade attack (level %d)" % character.attack_level
		$Mining.text = "Upgrade mining (level %d)" % character.mining_level
		$Movement.text = "Upgrade movement (level %d)" % character.movement_level
	else:
		visible = false

func _upgrade_attack():
	if game.resources_owned >= 1:
		game.resources_owned -= 1
		game.characters[num - 1].attack_level += 1
		emit_signal("resource_consumed")

func _upgrade_mining():
	if game.resources_owned >= 1:
		game.resources_owned -= 1
		game.characters[num - 1].mining_level += 1
		emit_signal("resource_consumed")

func upgrade_movement():
	if game.resources_owned >= 1:
		game.resources_owned -= 1
		game.characters[num - 1].movement_level += 1
		emit_signal("resource_consumed")
