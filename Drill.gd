extends Spatial

onready var door_animations = [$AnimateDoor1, $AnimateDoor2]

func open_doors():
	for anim in door_animations:
		anim.play("door")

func close_doors():
	for anim in door_animations:
		anim.play_backwards("door")
