extends Spatial

const RAYCAST_MAX_DISTANCE = 10000
const PAN_SPEED = 4.0
const PAN_MOUSE_SPEED = 0.1

const RESOURCE_NEEDED = 10.0

const Swarmer = preload("res://Swarmer.tscn")

const MAPS = ["res://Map1.tscn", "res://Map2.tscn", "res://Map3.tscn", "res://Map4.tscn", "res://Map5.tscn"]

onready var camera = $CameraBase/Camera
onready var map: Navigation = $Map

var resources_owned = 0.0

var current_level = 1
var current_level_time = 0.0
var next_wave = null

# Stats
var resources_max = 15.0

var middle_pressed = false

var selected_characters = []
onready var characters = [$Character1, $Character2, $Character3]
var character_starting_positions = []

func _ready():
	get_tree().paused = true
	$TransitionScreen.visible = true
	$TransitionScreen/StartScreen.visible = true
	$TransitionScreen/Upgrade.visible = false
	$"Characters/char1".character = $Character1
	$"Characters/char2".character = $Character2
	$"Characters/char3".character = $Character3

	# Save character starting positions for next levels
	for character in characters:
		character_starting_positions.append(character.translation)

func _process(delta):
	if Input.is_action_pressed("pan_left"):
		$CameraBase.translate(Vector3(-1, 0, 0) * PAN_SPEED)
	if Input.is_action_pressed("pan_right"):
		$CameraBase.translate(Vector3(1, 0, 0) * PAN_SPEED)
	if Input.is_action_pressed("pan_up"):
		$CameraBase.translate(Vector3(0, 0, -1) * PAN_SPEED)
	if Input.is_action_pressed("pan_down"):
		$CameraBase.translate(Vector3(0, 0, 1) * PAN_SPEED)

	if Input.is_action_just_pressed("drill_down"):
		_on_drill_down_pressed()

func _physics_process(delta):
	current_level_time += delta

	if Input.is_action_just_pressed("character1"):
		select_character(1)
	elif Input.is_action_just_pressed("character2"):
		select_character(2)
	elif Input.is_action_just_pressed("character3"):
		select_character(3)

	if next_wave == null or next_wave <= current_level_time:
		# Select a random spawn point
		var spawn_points = get_tree().get_nodes_in_group("SpawnPoints")
		var spawn = spawn_points[randi() % len(spawn_points)]

		# Spawn enemies
		var num_enemies = 4 * pow(1.15, current_level - 1) * pow(1.5, current_level_time / 60.0)
		var radius = 5.0 * sqrt(num_enemies)
		for i in range(num_enemies):
			var enemy = Swarmer.instance()
			enemy.translation = spawn.translation + Vector3(rand_range(-radius, radius), 0, rand_range(-radius, radius))
			enemy.rotation.y = rand_range(0, 2 * PI)
			map.add_child(enemy)

		# Pick next wave time
		next_wave = current_level_time + 30.0 * pow(0.9, current_level - 1)

	var alive = false
	for character in characters:
		if character.health > 0.0:
			alive = true

	if not alive and $LossTimer.is_stopped():
		$LossTimer.start()

func _unhandled_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and not event.pressed:
		# Find target in 3D space
		var from = camera.project_ray_origin(event.position)
		var to = from + camera.project_ray_normal(event.position) * RAYCAST_MAX_DISTANCE
		var target_point = map.get_closest_point_to_segment(from, to)

		# Pick resources
		var best_sq_dist = INF
		var best_rsrc = null
		var best_enemy = null
		for rsrc in get_tree().get_nodes_in_group("Resources"):
			var sq_dist = (rsrc.translation - target_point).length_squared()
			if sq_dist < 12.0 and sq_dist < best_sq_dist:
				best_rsrc = rsrc
				best_enemy = null
				best_sq_dist = sq_dist

		# Pick enemy
		for enemy in get_tree().get_nodes_in_group("EnemyTargets"):
			var sq_dist = (enemy.translation - target_point).length_squared()
			if sq_dist < 9.0 and sq_dist < best_sq_dist:
				best_enemy = enemy
				best_rsrc = null
				best_sq_dist = sq_dist

		if best_rsrc:
			# Pick up resources
			for character in selected_characters:
				character.pick_up_resources(best_rsrc)
		elif best_enemy:
			# Attack enemy
			for character in selected_characters:
				character.attack_enemy(best_enemy)
		else:
			# Path finding
			for character in selected_characters:
				character.move_to(target_point)

	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and not event.pressed:
		# Find target in 3D space
		var from = camera.project_ray_origin(event.position)
		var to = from + camera.project_ray_normal(event.position) * RAYCAST_MAX_DISTANCE
		var target_point = map.get_closest_point_to_segment(from, to)

		# Pick character
		var best_sq_dist = INF
		var best_character = null
		for character in characters:
			if character.health > 0.0:
				var sq_dist = (character.translation - target_point).length_squared()
				if sq_dist < 9.0 and sq_dist < best_sq_dist:
					best_sq_dist = sq_dist
					best_character = character

		if best_character != null:
			select_character(best_character.num)

	# Pan with the mouse
	if event is InputEventMouseButton and event.button_index == BUTTON_MIDDLE:
		middle_pressed = event.pressed

	if event is InputEventMouseMotion and middle_pressed:
		$CameraBase.translate(Vector3(-event.relative.x, 0, -event.relative.y) * PAN_MOUSE_SPEED)

func select_character(num):
	print("Selecting %d" % num)
	for character in selected_characters:
		character.selected = false
	selected_characters = [characters[num - 1]]
	for character in selected_characters:
		character.selected = true

func get_resources(amount):
	resources_owned = clamp(resources_owned + amount, 0, resources_max)
	$TopRight/Resources.text = "Resource: %d" % resources_owned
	$GoButton/Button.visible = resources_owned >= RESOURCE_NEEDED

func _on_start_pressed():
	if get_tree().paused:
		for character in characters:
			character.visible = false
		$TransitionAnimation.play("LevelStart")
		$TransitionScreen/StartScreen.visible = false
		camera.clear_current(false)
		$TransitionCamera.make_current()

func _on_game_start():
	get_tree().paused = false
	$TransitionScreen.visible = false
	$CameraBase.translation = Vector3(0, 0, -15.4)
	$TransitionCamera.clear_current(false)
	camera.make_current()
	for i in len(characters):
		var character = characters[i]
		if character.health > 0.0:
			character.visible = true
			character.translation = character_starting_positions[i]

			# Heal to full health
			character.health = character.max_health
		else:
			character.visible = false
			character.translation = Vector3(-1000, 0, 0)
	$TopRight/Resources.text = "Resource: %d" % resources_owned
	$GoButton/Button.visible = resources_owned >= RESOURCE_NEEDED

func _on_drill_down_pressed():
	if resources_owned < RESOURCE_NEEDED:
		return
	resources_owned -= RESOURCE_NEEDED
	$GoButton/Button.visible = false

	get_tree().paused = true
	for character in characters:
		character.visible = false
		character.mining_target = null
		character._unset_target()
		character.order = null

	camera.clear_current(false)
	$TransitionCamera.make_current()
	$TransitionScreen.visible = true
	$TransitionAnimation.play("LevelEnd")

func _on_level_end():
	$TransitionScreen/Upgrade.visible = true
	$TransitionScreen/Upgrade.update_info()
	map.queue_free()
	current_level += 1
	current_level_time = 0.0
	next_wave = null
	var map_name = MAPS[randi() % len(MAPS)]
	map = load(map_name).instance()
	add_child(map)

func _on_loss():
	$LossScreen.visible = true
	if current_level == 1:
		$LossScreen/Score.visible = false
	else:
		$LossScreen/Score.text = "You made it down %d levels!" % (current_level - 1)
