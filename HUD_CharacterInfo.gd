extends Control

var character = null setget _set_character

func _set_character(new_character):
	character = new_character
	if is_inside_tree():
		$Num.text = "%d." % character.num

func _ready():
	if character:
		$Num.text = "%d." % character.num

func _on_back_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		var game = $"../.."
		game.select_character(character.num)

func _process(delta):
	if character == null:
		visible = false
	elif character.health <= 0:
		visible = true
		$Dead.visible = true
		$Health.visible = false
		$Selected.visible = false
	else:
		$Selected.visible = character.selected
		$Dead.visible = false
		$Health.visible = true
		$Attack.text = "attack: %d" % character.attack_level
		$Health.text = "health: %d" % character.health
