extends Control

export(NodePath) var character_path setget _set_character

var character

func _set_character(new_character_path):
	character_path = new_character_path
	if is_inside_tree():
		character = get_node(character_path)

func _ready():
	character = get_node(character_path)
	$Name.text = "%d." % character.num

func _process(delta):
	if not character.visible:
		visible = false
	else:
		visible = true

		if character.selected:
			$Bar/Value.set("custom_colors/font_color", Color(1.0, 1.0, 1.0))
		else:
			$Bar/Value.set("custom_colors/font_color", Color(0.0, 0.0, 0.0))

		var health = character.health
		if health <= 0.0:
			health = 0.0

		$Bar.max_value = character.max_health
		$Bar.value = health

		$Bar/Value.text = "%d/%d" % [health, character.max_health]

		var camera = get_tree().root.get_camera()
		rect_position = camera.unproject_position(character.translation)
