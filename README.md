Drill Deeper
============

Submission to Ludum Dare 48 Jam, a 72-hour game-making event. [You can find the submission page here](https://ldjam.com/events/ludum-dare/48/$241179).

This is my first entry to a game jam. I didn't get to spend the whole weekend on it, but I am still happy I managed to submit in time for the jam. I wish I'd had more time to make 3D models.

The game was created using [Godot](https://godotengine.org/) 3.3, [Blender](https://www.blender.org/) 2.82, and [sfxr](http://www.drpetter.se/project_sfxr.html).

![screenshot](screenshots/Screenshot_20210426_161517.png)
