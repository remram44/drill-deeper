extends Spatial

const CUBE = preload("res://ResourceCubeRed.tscn")

export(float) var value = 5

var piles = []

func _ready():
	for i in range(9):
		piles.append([])
	for i in range(value):
		# Select a pile
		var pile = randi() % 9
		# Make a cube
		var cube = CUBE.instance()
		cube.translation.x = floor(pile / 3) - 1
		cube.translation.z = pile % 3 - 1
		cube.translation.y = len(piles[pile])
		add_child(cube)
		piles[pile].append(cube)

func pick_up():
	if value <= 0:
		pass
	elif value == 1:
		# Remove remaining cubes
		for pile in range(9):
			for cube in piles[pile]:
				cube.queue_free()
			piles[pile] = []
		$RemoveTimer.start()
		$"../..".get_resources(1)
		value = 0
		$Empty.play()
	else:
		value -= 1
		# Remove a cube
		while true:
			var pile = piles[randi() % 9]
			if len(pile) > 0:
				pile.pop_back().queue_free()
				break
		$Pickup.play()

		$"../..".get_resources(1)
