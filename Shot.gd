extends Spatial

func setup(from, to):
	# Position at start
	translation = from

	# Orient towards end
	look_at(to, Vector3(0, 1, 0))

	var dist = (to - from).length()

	# Stretch trail
	$Trail.scale.z = dist

	# Position hit effect
	$HitBase.translation.z = -dist

	# Start effects
	$ShootBase/ShootParticles.one_shot = true
	$ShootBase/ShootParticles.emitting = true

	$HitBase/HitParticles.one_shot = true
	$HitBase/HitParticles.emitting = true
