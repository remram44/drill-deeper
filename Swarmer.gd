extends KinematicBody

var health = 12

# Stats
var detection_radius = 50.0
var turn_speed = 0.05
var move_speed = 12.0
var attack_damage = 10.0
var max_health = 28.0

func _ready():
	health = max_health

func damage(amount):
	health -= amount
	if health <= 0.0:
		queue_free()

func _physics_process(delta):
	# Find closest target
	var target = null
	var best_sq_dist = null
	for possible_target in get_tree().get_nodes_in_group("AlliedTargets"):
		if possible_target.get("health") == null or possible_target.get("health") <= 0:
			continue
		var sq_dist = (possible_target.translation - translation).length_squared()
		if target == null or sq_dist < best_sq_dist:
			target = possible_target
			best_sq_dist = sq_dist

	# Only target it if within range
	if target and best_sq_dist > detection_radius * detection_radius:
		target = null

	if target:
		var direction = target.translation - translation

		# Turn towards it
		var bearing = atan2(-direction.x, -direction.z)
		var turn = fmod(bearing - rotation.y + 3 * PI, 2 * PI) - PI
		turn = clamp(turn, -turn_speed, turn_speed)
		rotation.y += turn

		# Attack?
		if best_sq_dist < 3.5 * 3.5:
			target.damage(attack_damage)
			translate_object_local(Vector3(0, 0, 5))
			$AttackSound.play()
		else:
			# Move towards it
			turn = fmod(bearing - rotation.y + 3 * PI, 2 * PI) - PI
			if abs(turn) < 0.08:
				move_and_slide(transform.basis.xform(Vector3(0, 0, -move_speed)))
