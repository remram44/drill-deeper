extends CenterContainer

onready var game = $"../.."

func update_info():
	$Control/Label.text = "After refueling your drill, you have %d resources left" % game.resources_owned
	$Control/Char1.update_info()
	$Control/Char2.update_info()
	$Control/Char3.update_info()
