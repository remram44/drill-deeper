Source files for the assets. Those are the files you can edit, that get "exported" into a Godot-friendly format in the parent directory.

* `.sfxr` files are sound effects created using [sfxr](http://www.drpetter.se/project_sfxr.html). Use the "load sound" button on the right to load those files
* `.blend` files are 3D models created with [Blender](https://www.blender.org/) 2.82
